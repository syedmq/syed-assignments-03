package com.pathashala57;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MarsRoverTest {

    @BeforeEach
    void initialise() {
        Coordinate.initialiseMap(5, 5);

    }


    @Test
    void checkGetPosition() {
        MarsRover newRover = new MarsRover(new Coordinate(1, 2, Integer.parseInt(Coordinate.map.get('N').toString())));
        assertEquals(newRover.getPosition(),"(1,2,N)");

    }
    @Test
    void checkRoverCommandExecution() {
        MarsRover newRover = new MarsRover(new Coordinate(1, 2, Integer.parseInt(Coordinate.map.get('N').toString())));
        newRover.getPosition();
        newRover.executeCommand('L');
        newRover.getPosition();
        assertTrue(newRover.roverCoordinates.equals(new Coordinate(1,2, Integer.parseInt(Coordinate.map.get('W').toString()))));

    }

}

