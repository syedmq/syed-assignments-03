package com.pathashala57;

import com.pathashala57.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CoordinateTest {

    @BeforeEach
    void initialise() {
        Coordinate.initialiseMap(5, 5);

    }

    @Test
    void checkRotateLeft() {
        int direction = Integer.parseInt(Coordinate.map.get('N').toString());
        Coordinate roverCorrdinates = new Coordinate(1, 1, direction);
        roverCorrdinates.getPosition();
        roverCorrdinates = roverCorrdinates.rotateLeft();
        roverCorrdinates.getPosition();
        assertTrue(roverCorrdinates.equals(new Coordinate(1, 1, Integer.parseInt(Coordinate.map.get('W').toString()))));

    }

    @Test
    void checkRotateRight() {
        int direction = Integer.parseInt(Coordinate.map.get('N').toString());
        Coordinate roverCorrdinates = new Coordinate(1, 1, direction);
        roverCorrdinates.getPosition();
        roverCorrdinates = roverCorrdinates.rotateRight();
        roverCorrdinates.getPosition();
        assertTrue(roverCorrdinates.equals(new Coordinate(1, 1, Integer.parseInt(Coordinate.map.get('E').toString()))));


    }

    @Test
    void checkMove() {
        int direction = Integer.parseInt(Coordinate.map.get('N').toString());
        Coordinate roverCorrdinates = new Coordinate(1, 1, direction);
        roverCorrdinates.getPosition();
        roverCorrdinates = roverCorrdinates.moveForward();
        roverCorrdinates.getPosition();
        assertTrue(roverCorrdinates.equals(new Coordinate(1, 2, Integer.parseInt(Coordinate.map.get('N').toString()))));


    }


}
