package com.pathashala57;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandParserTest {
    @BeforeEach
    void initialise() {
        Coordinate.initialiseMap(5, 5);

    }

    @Test
    void checkRoverCommandParser() {
        MarsRover newRover = new MarsRover(new Coordinate(1, 2, Integer.parseInt(Coordinate.map.get('N').toString())));
        newRover.getPosition();
        CommandParser parser = new CommandParser("LMMLRM");
        parser.commandsExecute(newRover);
        assertEquals(newRover.getPosition(),"(0,2,W)");

    }

    @Test
    void checkCommandParserwithInavlidCommands() {
        MarsRover newRover = new MarsRover(new Coordinate(1, 2, Integer.parseInt(Coordinate.map.get('N').toString())));
        newRover.getPosition();
        CommandParser parser = new CommandParser("RMMMMM MMLMMM");
        parser.commandsExecute(newRover);
        newRover.getPosition();

    }

}
