package com.pathashala57;

// Represents the point on plateu with direction

import java.util.HashMap;
import java.util.Map;

public class Coordinate {

    static int xmax;
    static int ymax;

    int x;
    int y;
    int direction;

    static Map map;

    Coordinate(int x, int y, int givenDirection) {

        this.x = x;
        this.y = y;
        this.direction = givenDirection;
    }


    public String getPosition() {

        String position="(" + x + "," + y + "," + map.get(direction).toString() + ")";
        System.out.println(position);

        return position;
    }

    static void initialiseMap(int x, int y) {

        xmax = x;
        ymax = y;
        map = new HashMap();
        map.put('N', 0);
        map.put('E', 1);
        map.put('S', 2);
        map.put('W', 3);
        map.put(0, 'N');
        map.put(1, 'E');
        map.put(2, 'S');
        map.put(3, 'W');
    }

    public Coordinate rotateLeft() {
        int newDirection = (this.direction + 3) % 4;

        return new Coordinate(this.x, this.y, newDirection);

    }


    public Coordinate rotateRight() {

        int newDirection = (this.direction + 1) % 4;

        return new Coordinate(this.x, this.y, newDirection);

    }

    public Coordinate moveForward() {
        int x = this.x;
        int y = this.y;
        switch (this.direction) {
            case 0:
                y = y + 1;
                break;
            case 1:
                x++;
                break;
            case 2:
                y--;
                break;
            case 3:
                x--;
                break;
        }

        if (x < 0 || y < 0 || y > ymax || x > xmax) {

            x = this.x;
            y = this.y;
            System.out.println("MarsRover Unable To move Forward Plateu ended");

        }
        return new Coordinate(x, y, this.direction);

    }

    @Override
    public boolean equals(Object that) {
        if (that == null)
            return false;
        if (that == this)
            return true;
        if (that.getClass() != Coordinate.class)
            return false;
        Coordinate anotherCoordinate = (Coordinate) that;
        if (anotherCoordinate.x == this.x && anotherCoordinate.y == this.y && anotherCoordinate.direction == this.direction) {
            return true;
        }
        return false;
    }
}
