package com.pathashala57;

//Understands commands and executes them
public class CommandParser {

    String commands;

    CommandParser(String commands) {
        this.commands = commands;
    }

    public void commandsExecute(MarsRover rover) {

        for (char command :
                commands.toCharArray()) {
            if (command == 'L' || command == 'M' || command == 'R') {
                rover.executeCommand(command);

            } else
                System.out.println("Invalid Command:" + command);


        }

    }
}
