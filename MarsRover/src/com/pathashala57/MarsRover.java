package com.pathashala57;

//Represents the mechanical machine
public class MarsRover {


    Coordinate roverCoordinates;

    MarsRover(Coordinate value) {
        roverCoordinates = value;
    }

    public void executeCommand(char command) {

        switch (command) {
            case 'L':
                roverCoordinates = roverCoordinates.rotateLeft();
                break;
            case 'R':
                roverCoordinates = roverCoordinates.rotateRight();
                break;
            case 'M':
                roverCoordinates = roverCoordinates.moveForward();
                break;
        }

    }

    public String getPosition() {
        return roverCoordinates.getPosition();
    }
}
